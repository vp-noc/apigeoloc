#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe APIGeoloc::CLI do
  context 'version' do
    it 'prints the version.' do
      out = "APIGeoloc version #{APIGeoloc::VERSION}\n"
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context 'match http://api.yueyehua.net/ 127.0.0.0/8 -s' do
    it 'Runs the simulation mode and do nothing.' do
      expect { start(self) }.to output('').to_stdout
    end
  end

  context 'notexistingcmd' do
    it 'is an unknown command and does nothing.' do
      out = "Could not find command \"notexistingcmd\".\n"
      expect { start(self) }.to output(out).to_stderr
    end
  end
end
