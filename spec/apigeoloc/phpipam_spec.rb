#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe APIGeoloc::Phpipam do # rubocop:disable Metrics/BlockLength
  opts_wsource = '-S notexistingsource'
  opts_auth = '-u user -p passwd'
  opts_wauth = '-u wrong -p passwd'

  out1 = "Expected '--source' to be one of phpipam; got notexistingsource\n"
  context "match http://api.yueyehua.net 127.0.0.0/8 #{opts_wsource}" do
    it 'tries unimplemented source.' do
      expect { start(self) }.to output(out1).to_stderr
    end
  end

  context 'match http://api.yueyehua.net 127.0.0.0.24' do
    it 'tries an invalid cidr subnet.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  out3 = 'Tour Eiffel, 5 Avenue Anatole France, 75007 Paris, ' \
    "[48.858193, 2.294446]\n"
  context "match http://api.yueyehua.net 127.0.0.0/24 #{opts_auth}" do
    it 'tries a user and a password to get a token.' do
      expect { start(self) }.to output(out3).to_stdout
    end
  end

  context "match http://api.yueyehua.net 127.0.0.0/24 #{opts_wauth}" do
    it 'tries a wrong user and a password.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context "match http://api.yueyehua.net 128.0.0.0/24 #{opts_auth}" do
    it 'tries an unknown subnet.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context "match http://api.yueyehua.net 129.0.0.0/24 #{opts_auth}" do
    it 'tries a subnet without location.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end
end
