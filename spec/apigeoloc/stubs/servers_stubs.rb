#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def readf(file)
  dir = File.dirname(__FILE__)
  File.read(File.join(dir, '..', '..', 'files', file))
end

RSpec.configure do |config|
  config.before(:each) do
    # basic requests an API
    stub_request(:post, 'http://api.yueyehua.net/')
      .to_return('status' => 200, 'body' => '{}', 'headers' => {})
    # request token with good credentials
    stub_request(:post, 'http://api.yueyehua.net/user/')
      .with(basic_auth: %w[user passwd])
      .to_return('status' => 200, 'body' => readf('gooduser'), 'headers' => {})
    # request token with wrong credentials
    stub_request(:post, 'http://api.yueyehua.net/user/')
      .with(basic_auth: %w[wrong passwd])
      .to_return('status' => 200, 'body' => readf('wuser'), 'headers' => {})
    # request a cidr
    stub_request(:get, 'http://api.yueyehua.net/subnets/cidr/127.0.0.0/24')
      .with('headers' => { 'Token' => 'THIS_IS_A_FAKE_TOKEN' })
      .to_return('status' => 200, 'body' => readf('cidr_127'), 'headers' => {})
    # request an unknown cidr
    stub_request(:get, 'http://api.yueyehua.net/subnets/cidr/128.0.0.0/24')
      .with('headers' => { 'Token' => 'THIS_IS_A_FAKE_TOKEN' })
      .to_return('status' => 200, 'body' => readf('cidr_128'), 'headers' => {})
    # request a cidr with unknown location
    stub_request(:get, 'http://api.yueyehua.net/subnets/cidr/129.0.0.0/24')
      .with('headers' => { 'Token' => 'THIS_IS_A_FAKE_TOKEN' })
      .to_return('status' => 200, 'body' => readf('cidr_129'), 'headers' => {})
    # request a location
    stub_request(:get, 'http://api.yueyehua.net/tools/locations/7/')
      .with('headers' => { 'Token' => 'THIS_IS_A_FAKE_TOKEN' })
      .to_return('status' => 200, 'body' => readf('loc_7'), 'headers' => {})
    # request an unknown location
    stub_request(:get, 'http://api.yueyehua.net/tools/locations/8/')
      .with('headers' => { 'Token' => 'THIS_IS_A_FAKE_TOKEN' })
      .to_return('status' => 200, 'body' => readf('loc_8'), 'headers' => {})
  end
end
