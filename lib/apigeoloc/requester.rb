#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'net/http'
require 'openssl'

module APIGeoloc
  # This is a Net HTTP requester class.
  class Requester
    # Class constructor method
    def initialize(method, url, opts)
      @method = method
      @uri = URI.parse(url)
      @opts = opts
      @http = Net::HTTP.new(@uri.host, @uri.port)
      @http.use_ssl = @uri.scheme.eql?('https')
      @http.verify_mode = OpenSSL::SSL::VERIFY_NONE unless @opts['insecure']
    end

    # Set the authentication data
    def basic_auth(request)
      unless @opts['user'].nil? || @opts['password'].nil?
        request.basic_auth(@opts['user'], @opts['password'])
      end
      request
    end

    # Set the headers given as argument
    def format_header(request)
      unless @opts['header'].nil?
        @opts['header'].split(',').each do |h|
          (key, value) = h.split(':')
          request[key] = value
        end
      end
      request
    end

    # Select request method
    def request
      case @method.upcase
      when 'GET' then rest_request_get
      when 'POST' then rest_request_post
      end
    end

    # GET method
    def rest_request_get
      req = Net::HTTP::Get.new(@uri.request_uri)
      req = format_header(req)
      @http.request(req)
    end

    # POST method
    def rest_request_post
      req = Net::HTTP::Post.new(@uri.request_uri)
      req = format_header(req)
      req = basic_auth(req)
      @http.request(req)
    end
  end
end
