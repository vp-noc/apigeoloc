#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'

module APIGeoloc
  # Simple CLI for apigeoloc
  class CLI < Thor
    desc 'version', 'Print apigeoloc current version'
    def version
      puts "APIGeoloc version #{APIGeoloc::VERSION}"
    end

    desc('match URL SUBNET [options]',
         'Send a request to get the full datas.' \
         '  URL: base url for the requested api.' \
         '  SUBNET: subnet to match as cidr subnet format.')
    option(
      :header,
      aliases: ['-h'],
      desc: 'Http headers used with the request (comma separated). ' \
            '<key>:<value>[,<key>:<value>[,...]]'
    )
    option(
      :user,
      aliases: ['-u'],
      desc: 'User name in order to login. See also password option.'
    )
    option(
      :password,
      aliases: ['-p'],
      desc: 'User password in order to login. See also user option.'
    )
    option(
      :source,
      aliases: ['-S'],
      default: 'phpipam',
      enum: ['phpipam'],
      desc: 'Select the matcher api source.'
    )
    option(
      :insecure,
      aliases: ['-k'],
      type: :boolean,
      default: false,
      desc: 'No Check certificates.'
    )
    option(
      :simulation,
      aliases: ['-s'],
      type: :boolean,
      default: false,
      desc: 'Simulation mode. Do nothing'
    )
    def match(url, cidr)
      opts = options.dup
      matcher = APIGeoloc::Matcher.new.send(opts['source'], url, cidr, opts) \
        unless opts['simulation']
      matcher.match unless opts['simulation']
    end
  end
end
