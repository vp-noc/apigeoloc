#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'json'

module APIGeoloc
  # This is a PHPIPAM requester class.
  class Phpipam
    # Class constructor method
    def initialize(apiurl, subnet, opts)
      cidr_regex = '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}' \
        '([0-9]|[1-9][0-9]|1[0-9]\{2\}|2[0-4][0-9]|25[0-5])' \
        '(\/([0-9]|[1-2][0-9]|3[0-2]))$'
      raise 'Invalid cidr subnet format' \
        unless subnet.match(Regexp.new(cidr_regex))
      @apiurl = apiurl.sub(%r{/^(.*)\/$/}, '\1')
      @subnet = subnet
      @opts = opts
      @opts['header'] = "TOKEN:#{receive_user_token}"
    end

    # Set the user token.
    def receive_user_token
      res = APIGeoloc::Requester.new('POST', "#{@apiurl}/user/", @opts).request
      body = JSON.parse(res.body)
      raise 'Invalid username or password' unless body['code'] == 200
      body['data']['token']
    end

    # Get Subnet location id.
    def subnet_loc_id
      sn_req_path = "#{@apiurl}/subnets/cidr/#{@subnet}"
      sn_res = APIGeoloc::Requester.new('GET', sn_req_path, @opts).request
      sn_body = JSON.parse(sn_res.body)
      check_subnet_data(sn_body['data'])
      sn_body['data'][0]['location']
    end

    # Raise error if the data or location id  doesn't exist.
    def check_subnet_data(data)
      raise 'Subnet not found' if data.nil?
      raise 'Location unknown' if data[0]['location'].nil?
    end

    # Get the location data.
    def location_data(id)
      loc_req_path = "#{@apiurl}/tools/locations/#{id}/"
      loc_res = APIGeoloc::Requester.new('GET', loc_req_path, @opts).request
      loc_body = JSON.parse(loc_res.body)
      raise 'Location not found' if loc_body['data'].nil?
      format_location(loc_body['data'])
    end

    # Transform json to a human readable string.
    def format_location(data)
      name = data['name']
      address = data['address']
      lat = data['lat']
      long = data['long']
      "#{name}, #{address}, [#{lat}, #{long}]"
    end

    # Match the subnet with a location if possible.
    def match
      location = location_data(subnet_loc_id)
      puts location
    end
  end
end
