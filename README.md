# APIGeoloc
[![License](https://img.shields.io/badge/license-Apache-blue.svg)](LICENSE)
[![build status](https://gitlab.com/vp-noc/apigeoloc/badges/develop/build.svg)](https://gitlab.com/vp-noc/apigeoloc/commits/develop)
[![coverage report](https://gitlab.com/vp-noc/apigeoloc/badges/develop/coverage.svg)](https://vp-noc.gitlab.io/apigeoloc/)
[![Gem Version](https://badge.fury.io/rb/apigeoloc.svg)](https://badge.fury.io/rb/apigeoloc)

1. [Overview](#overview)
2. [Description](#role-description)
3. [Setup](#setup)
4. [Usage](#usage)
5. [Limitations](#limitations)
6. [Development](#development)
7. [Miscellaneous](#miscellaneous)

## Overview

`APIGeoloc` is a simple tool to match a subnet with a location.

## Description

This tool request the reference tool containing the datas to match an ip with a
location.

## Setup

    $ gem install apigeoloc

## Usage

An interactive help is available with:

    $ apigeoloc help

## Examples

To match a subnet with a location:

    $ apigeoloc match "<API_URL>" "<CIDR_SUBNET>" -u "<USER>" -p "<PASSWORD>"

## Limitations

It is currently only available to request phpipam.

## Development

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

```
    ╚⊙ ⊙╝
  ╚═(███)═╝
 ╚═(███)═╝
╚═(███)═╝
 ╚═(███)═╝
  ╚═(███)═╝
   ╚═(███)═╝
```
